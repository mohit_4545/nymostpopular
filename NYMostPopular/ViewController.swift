//
//  ViewController.swift
//  NYMostPopular
//
//  Created by Mohit on 3/25/19.
//  Copyright © 2019 Mohit. All rights reserved.
//

import UIKit
import SDWebImage
import ANLoader

class ViewController: UIViewController {
    let jsonParserObj = RTJsonParser()
    var DictToSend: [Dictionary<String,Any>]?
    
    @IBOutlet var copyrightLbl: UILabel!
    
    @IBOutlet weak var articleTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        jsonParserObj.delegate = self
        // Do any additional setup after loading the view, typically from a nib.
        self.addShadoow()
        self.callMostpopularArticle()
    }
    
    
    func addShadoow(){
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.black.cgColor
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.navigationController?.navigationBar.layer.shadowRadius = 4.0
        self.navigationController?.navigationBar.layer.shadowOpacity = 1.0
        self.navigationController?.navigationBar.layer.masksToBounds = false
    }
    
    func callMostpopularArticle(){
        
        if AppUtility.isConnectedToNetwork() {
            ANLoader.showLoading("Loading...", disableUI: true)
            
            let inputValue = ""
            jsonParserObj.ParseMyJson(url: URL.init(string:NYStrings.kMostPopularArticleAPIURL)! , param: inputValue as AnyObject, httpMethod: NYStrings.kMethod_Get, contentType: NYStrings.kContentType_urlencoded , mScreen: "MostPopularArticleAPIURL", authStr: nil)
            
        }
        else{
            AppUtility.customAlert(msg: "Please check your Internet!", customView: self.view)
        }
    }
    
}


extension  ViewController: RTJsonParserDelegate{
    func ResponseFromServer(withSuccess Success: Bool, json: AnyObject?, screen: String, Error: String?){
        print("screen MostPopularArticleAPIURL:- \(screen)")
        print("Success :- \(Success)")
        if Success == false{
            DispatchQueue.main.async{
                ANLoader.hide()
            }
        }
        
        if Success == true && screen == "MostPopularArticleAPIURL" {
            DispatchQueue.main.async {
                if json != nil{
                    if  let responseDic = json as? Dictionary<String,Any>{
                        
                        
                        if let ResponseArray = responseDic["results"] as? [Dictionary<String,Any>]{
                            self.DictToSend = ResponseArray
                            self.copyrightLbl.text = responseDic["copyright"] as? String
                        }
                        
                        print("screen MostPopularArticleAPIURL:- \(screen)")
                        print(responseDic)
                        
                        ANLoader.hide()
                        self.articleTableView.delegate = self
                        self.articleTableView.dataSource = self
                        self.articleTableView.reloadData()
                    }
                    
                }
                else{
                    
                    ANLoader.hide()
                    
                }
            }
            
        }
    }
    
    
    
    
    
}







extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.DictToSend!.count
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CustomCell
        cell.populateData(response: self.DictToSend![indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
                if  let viewController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "NYWebData") as? NYWebData{
                    if let strUrl = self.DictToSend![indexPath.row] as? Dictionary<String,Any>{
                        viewController.urlStr = strUrl["url"] as? String
                        viewController.title = strUrl["section"] as? String
                        
                    }
                    self.navigationController?.pushViewController(viewController, animated: true)
                }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}




class CustomCell: UITableViewCell {
    
    @IBOutlet var ImageView: UIImageView!
    
    @IBOutlet var TitleLbl: UILabel!
    @IBOutlet var abstractLbl: UILabel!
    @IBOutlet var DateLbl: UILabel!
    @IBOutlet var NameLbl: UILabel!
    
    func textDropShadow() {
        self.layer.masksToBounds = false
        self.layer.shadowRadius = 2.0
        self.layer.shadowOpacity = 0.2
        self.layer.shadowOffset = CGSize(width: 1, height: 2)
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    func populateData(response: Dictionary<String,Any>){
        self.TitleLbl.text = response["title"] as? String
        self.abstractLbl.text = response["abstract"] as? String
        self.NameLbl.text = "section : \(response["section"] as? String ?? "")"
        self.DateLbl.text = "Date : \(response["published_date"] as? String ?? "")"
        
        if let media = response["media"] as? [Dictionary<String,Any>]{
            if let mediameta = media[0]["media-metadata"] as? [Dictionary<String,Any>]{
                if let imageUrl = mediameta[0]["url"] as? String {
                    self.ImageView.sd_setImage(with: URL(string: imageUrl))
                }
            }
        }
        
    }
}
