//
//  NYWebData.swift
//  NYMostPopular
//
//  Created by Mohit on 3/25/19.
//  Copyright © 2019 Mohit. All rights reserved.
//

import UIKit
import WebKit

class NYWebData: UIViewController {

    var urlStr: String?
    @IBOutlet var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let url = URL(string: urlStr!)!
        webView.load(URLRequest(url: url))
        
        // 2
        let refresh = UIBarButtonItem(barButtonSystemItem: .refresh, target: webView, action: #selector(webView.reload))
        toolbarItems = [refresh]
        navigationController?.isToolbarHidden = false
    }
}

extension NYWebData:WKNavigationDelegate{
    
    override func loadView() {
        webView = WKWebView()
        webView.navigationDelegate = self
        view = webView
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        title = webView.title
    }
}
