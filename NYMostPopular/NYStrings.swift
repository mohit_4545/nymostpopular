//
//  constant.swift
//  NYDriver
//
//  Created by Mohit on 3/15/19.
//  Copyright © 2019 Mohit. All rights reserved.
//


import UIKit
import Reachability

enum UrlTypes {
    case productionUrl, testUrl
}

let serverUrl: UrlTypes = .productionUrl

class NYStrings {
    
    struct NYServerUrl {
        
        static let MostPopularArticleProdUrl = "https://api.nytimes.com/svc/mostpopular/v2/mostemailed/all-sections/7.json?api-key=Zgm7UmNtEA6kmKDAv6GThtWcEchUg3GX"
        
        static let MostPopularArticleTestUrl = "https://api.nytimes.com/svc/mostpopular/v2/mostemailed/all-sections/7.json?api-key=Zgm7UmNtEA6kmKDAv6GThtWcEchUg3GX"
        
    }
    
    #if DEBUG
    static let kMostPopularArticleAPIURL         = serverUrl == .productionUrl ? NYServerUrl.MostPopularArticleProdUrl : NYServerUrl.MostPopularArticleTestUrl
    
    #else
    //Live Server
    static let kMostPopularArticleAPIURL         = NYServerUrl.LoginProdUrl
    
    #endif
    
    //URLs
    static let kContentType_Json = "application/json"
    static let kContentType_urlencoded = "application/x-www-form-urlencoded"
    static let kMethod_Get = "GET"
    static let kMethod_Post = "POST"
    static let kMethod_Put = "PUT"
    
    
    struct ErrorMessages {
        static let internetError = "No Internet Connection"
    }
}


class AppUtility: NSObject {
    
    class func showToastlocal(message : String, view: UIView) {
        let toastView = UIView(frame: CGRect(x: 20, y: (view.frame.height/2)-40, width: view.frame.width-40, height: 80))
        toastView.layer.cornerRadius = 20
        toastView.layer.masksToBounds = true
        let toastLabel = UILabel()
        toastLabel.frame = CGRect(x: 10, y: 10, width: toastView.frame.size.width-20, height: 60)
        toastLabel.text = message
        //        toastLabel.baselineAdjustment = .alignCenters
        toastLabel.lineBreakMode = .byClipping
        toastLabel.numberOfLines = 0
        toastLabel.font = UIFont(name: "Arial",size: 14.0)
        toastLabel.textAlignment = .center
        toastLabel.textColor = .white
        toastView.backgroundColor = UIColor.black
        view.bringSubviewToFront(toastLabel)
        toastView.addSubview(toastLabel)
        view.addSubview(toastView)
        UIView.animate(withDuration: 4, delay: 1, options: .curveEaseOut, animations: {
            toastView.alpha = 0.0
        }, completion: {(isCompleted) in
            toastView.removeFromSuperview()
        })
    }
    
    
    class func isConnectedToNetwork() -> Bool {
        do {
            let reachability = Reachability()!
            switch reachability.connection{
            case .wifi, .cellular:
                print("Connected With wifi")
                return true
            case .none:
                print("Not Connected")
                return false
            }
        }
    }
    
    class func customAlert(msg: String,customView:UIView){
        
        AppUtility.showToastlocal(message: msg,view: customView)
        
    }
    //MARK: - Class Method
    class  func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    class  func convertToArrDictionary(text: String) -> [AnyObject]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [AnyObject]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
}

@IBDesignable
class CardView: UIView {
    
    @IBInspectable var cornerRadius: CGFloat = 2
    
    @IBInspectable var shadowOffsetWidth: Int = 0
    @IBInspectable var shadowOffsetHeight: Int = 3
    @IBInspectable var VshadowColor: UIColor? = UIColor.black
    @IBInspectable var shadowOpacity: Float = 0.5
    
    override func layoutSubviews() {
        layer.cornerRadius = cornerRadius
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        
        layer.masksToBounds = false
        layer.shadowColor = VshadowColor?.cgColor
        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        layer.shadowOpacity = shadowOpacity
        layer.shadowPath = shadowPath.cgPath
    }
    
}
